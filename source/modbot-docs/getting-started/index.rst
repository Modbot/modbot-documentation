================
Getting Started
================


You're new to Modbot. Welcome. Its a pleasure to bring a new robot builder online. Follow the classes below to familiarise yourself with the Modbot Platform.

.. toctree::
   :maxdepth: 3

   overview
   quick-start
   build-and-train-with-modbot-composer
   develop-first-application
