Hardware Modules
=================

Servo
-------

Description
~~~~~~~~~~~~~~~~~~~~~~~~~~~
The Servo provides rotary motion for the Modbot platform. It is an integrated motor, transmission, bearings and controller unit.

It provides absolute position information, has an onboard IMU as well as current and temperature sensors.

.. image:: _images/servo.png
	:scale: 60%
	:align: right

Details
~~~~~~~~~~~

=========  ===================
       ID  B075-SRVO-100N-C000
Part Name  Servo
  Version  C000
=========  ===================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

=======================   ================================
Dimensions
Length (mm)               62.5
Diameter (mm)             75
Mechanical Properties
Material                  Aluminum & Stainless Steel
Mass (kg)                 0.85
Motor Type                3 Phase BLDC
Electrical Properties
Voltage (Limits)          12-50 VDC
Voltage (Nominal)         48 VDC
Power (Nominal)           Motor: 960 W
Current (Peak)            20 A
Electromechanical
Torque (Rated)            92 Nm
Rotational Speed (Max)    60 rpm @ 48 VDC
Communications
Protocol                  Ethercat
Physical Layer            2 x 18 AWG Twisted Pair
Control Frequency         1 kHz
Performance
Precision                 17-bit Absolute/18-bit Multiturn
Temperature (Rated Max)   130 degC
Loudness (Max)            60 dB
Interfaces
Mechanical                CMC
Electrical                EBO Connector
=======================   ================================

Adapter
-------------

Description
~~~~~~~~~~~~~~

The Adapter is a passive joint. Some joints do not need rotary motion. For those joints, an Adapter connects Modbot System Modules together.

.. image:: _images/adapter.png
	:scale: 60 %
	:align: right

Details
~~~~~~~~~~~

=========  ===================
       ID  B075-ADPT-MM01-A000
Part Name  Adapter
  Version  A000
=========  ===================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

======================  ==============================
Dimensions
Length (mm)             55
Diameter (mm)           70
Mechanical Properties
Material                Aluminum & Stainless Steel
Mass (kg)               0.516
Interfaces
Mechanical              CMC
======================  ==============================

Mechanical Breakout
--------------------

Description
~~~~~~~~~~~~~~

The Mechanical Breakout adapts any Servo to ISO standard mounting holes for adapter plates and mechanical coupling. The mounting pattern has a 50 mm Pitch Circle Diameter with 4 M6 threaded holes.

The outer diameter of the mounting surface is 63 mm and the diameter of the cutout hole on the mounting surface is 31.5 mm. There is a 6 mm diameter locating pin along the pitch circle. This can be used for attaching custom and off-the-shelf end of arm tools.

.. image:: _images/mechanical-breakout.png
	:scale: 45 %
	:align: right

Details
~~~~~~~~~~~

=========  ===================
       ID  B075-MBKT-0000-B001
Part Name  Mechanical Breakout
  Version  B001
=========  ===================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

=======================   ================================
Dimensions
Diameter (mm)             75
Length (mm)               37.5
Mechanical Properties
Material                  Aluminum
Mass (kg)                 0.12
Interfaces
Mechanical                CMC
=======================   ================================

HMI
-----

Description
~~~~~~~~~~~~~~

The HMI (Human-Machine-Interface) module gives users another way to interface with the Modbot system. The robot can be hand-guided through a series of points using Mannequin Mode, which are recorded by tapping the button on the HMI module. The hole pattern is the same as on the Mechanical Breakout.

.. image:: _images/HMI.png
	:scale: 60 %
	:align: right

Details
~~~~~~~~~~~

=========  ===================
       ID  B075-HMIU-1BTN-A001
Part Name  HMI
  Version  A001
=========  ===================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~
=======================   ================================
Dimensions
Diameter (mm)             75
Length (mm)               75
Mechanical Properties
Material                  Aluminum
Mass (kg)                 0.23
Interfaces
Mechanical                CMC
=======================   ================================

Bend-L
-------

Description
~~~~~~~~~~~~~~

The Bend connects to any Servo to form joints such as elbows in a Robot Arm. The L-type provides a 90 degree turn. Engravings distinguish the two sides to make assembly intuitive.

The L-type is used for Collaborative builds of robot arms.

.. image:: _images/L-bend.png
	:scale: 70 %
	:align: right

Details
~~~~~~~~~~~

=========  ===================
       ID  B075-BEND-LTYP-B001
Part Name  Bend (L-Type)
  Version  B001
=========  ===================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

=======================   ================================
Dimensions
Diameter (mm)             75
Length (mm)               110
Width (mm)                75
Height (mm)               75
Mechanical Properties
Material                  Aluminum
Mass (kg)                 0.27
Interfaces
Mechanical                CMC
=======================   ================================

Straight
---------

Description
~~~~~~~~~~~~~~

The Straight acts a bit like bones in a skeleton. Giving the robot form and reach. The Straight carries power and data from one end to the other automatically. Just connect it to any Servo. Engravings distinguish the two sides to make assembly intuitive.

One side of the Straight has the Male connector. The other is Female, which can attach to either a Servo or an Adapter.

.. image:: _images/straight.png
	:scale: 70 %
	:align: right

Details
~~~~~~~~~~~

=========  ===================
       ID  B075-STRT-3D01-B001
Part Name  Straight
  Version  B001
=========  ===================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

=======================   ================================
Dimensions
Diameter (mm)             75
Length (mm)               248.75
Mechanical Properties
Material                  Aluminum
Mass (kg)                 0.56
Interfaces
Mechanical                CMC
=======================   ================================

Base
-----

Description
~~~~~~~~~~~~~~

The Base is provides a stable mechanical interface to couple to a robot build. This base is useful for Robot Arm configurations.

The Base is 80/20 compatible with the M8 bolt holes spaced in an 80mm square pattern. The base has external ports for both Power and Communications into the robot.

.. image:: _images/Base.png
	:scale: 50 %
	:align: right

Details
~~~~~~~~~~~

=========  ===================
       ID  B075-BASE-BARE-B001
Part Name  Base
  Version  B001
=========  ===================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

=======================   ================================
Dimensions
Length (mm)               118
Height (mm)               37.5
Diameter (mm)             75
Mechanical Properties
Material                  Aluminium
Mass (kg)                 0.17
Interfaces
Mechanical                CMC
Electrical                XT60
Communications            RJ45
=======================   ================================

Master Controller
-------------------

Description
~~~~~~~~~~~~~~

The Modbot Master Controller [insert description]. It comes with the necessary cable harness to connect to a Modbot robot.

.. image:: _images/master-controller.png
	:scale: 45 %
	:align: right

Details
~~~~~~~~~~~

=========  ===================
       ID  B000-BRAN-0000-C000
Part Name  Brain
  Version  C000
=========  ===================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

=======================   ================================
Interfaces
Electrical                US Power Plug
Communications            RJ45
=======================   ================================

Assembly Tool
----------------

Description
~~~~~~~~~~~~~~

The Assembly Tool is the only tool you’ll need to assemble Modbot System Modules together. It interfaces with the Modbot Common Mechanical Coupling (CMC).

.. image:: _images/assembly-tool.png
	:scale: 15 %
	:align: right

Details
~~~~~~~~~~~

=========  ===========================
       ID  Modbot Assembly Tool A001
Part Name  Assembly Tool
  Version  A001
=========  ===========================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

=======================   ================================
Dimensions
Length (mm)               330
Width (mm)                72
Mechanical Properties
Material                  Stainless Steel
Mass (kg)                 0.23
=======================   ================================

Gripper
----------

Description
~~~~~~~~~~~~~~

The gripper mounts to the end of a Modbot system either directly to a servo via the CMC interface or to a Mechanical Breakout via an adaptor piece. It uses parallel jaws to grip parts from the inside and outside. Users can make custom jaws to easily mount to the sliding rails.


.. image:: _images/gripper.png
	:scale: 50 %
	:align: right

Details
~~~~~~~~~~~

=========  ============================
       ID  B075-EOAT-PDFG-B001
Part Name  Parallel Dual Finger Gripper
  Version  B001
=========  ============================

Technical Specifications
~~~~~~~~~~~~~~~~~~~~~~~~~~

=======================   ================================
Dimensions
Diameter (mm)             75
Mechanical Properties
Mass (kg)                 2.5
Interfaces
Mechanical                CMC
=======================   ================================
