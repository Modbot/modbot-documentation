Kits
==========

6DoF-C
-------

Description
~~~~~~~~~~~~~~~~~~~~~~~~~~~
The 6DOF-C is a 6 degree of freedom collaborative ready robot configuration. The kit comes with everything you need to assemble your robot and start programming it.

.. image:: _images/6-dof-c.png
	:scale: 40 %
	:align: left
