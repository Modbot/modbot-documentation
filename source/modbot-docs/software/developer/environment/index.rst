Setting up a Development Environment
-------------------------------------

Whether you want to develop a client application to communicate with the Modbot Brain, or create your own Software Module to extend the Brain's capabilities, Modbot provides tools to quickly get you up and running with the required development environment for your application.

Hardware Requirements
~~~~~~~~~~~~~~~~~~~~

- WIP


The Modbot Launcher
~~~~~~~~~~~~~~~~~~~~

The Modbot Launcher tool provides a convenient way to install any packages and libraries necessary to develop Software Modules and Plugs. Provided you meet the hardware requirements, simply download the Launcher and run it to install any desired resources.


Libraries and Packages
~~~~~~~~~~~~~~~~~~~~~~~

If you prefer to configure your environment manually, please consult our required packages and libraries below:

- WIP
- WIP
- WIP