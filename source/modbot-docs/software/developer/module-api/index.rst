====================
The Module API
====================

You can extend the Modbot Brain's capabilities by creating custom Software Modules.
Modbot provides Brain Module and Plug interfaces, which can be extended to implement new modules.


Plug Modules
~~~~~~~~~~~~~

[ TBD: A Plug is a type of Software Module that enables the Brain to communicate with additional hardware. A Plug is essentially a modular driver for the Brain.
You might load the Modbot Servo Plug to control a Modbot Servo, or develop your own plug to extend the Brain's capabilities to communicate with your custom end-effector. ]
A plug should only perform one function; to extend the capabilities of the Brain to communicate with additional hardware.

Examples for a typical Software Plug and behavioral Module are provided below.

.. toctree::
   :maxdepth: 2
  
   module-api-plug-example
   module-api-example
   module-api-documentation