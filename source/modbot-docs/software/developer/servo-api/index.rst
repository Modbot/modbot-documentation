The Servo API
--------------

If you prefer to control the Modbot servos directly, you may bypass the Modbot Brain completely and control our servos