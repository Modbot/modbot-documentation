======
Modbot
======

Welcome to the Modbot Platform. Lets get you building robots.

.. toctree::
   :maxdepth: 1

   modbot-docs/getting-started/index
   modbot-docs/conventions/index
   modbot-docs/hardware/index
   modbot-docs/software/index
   modbot-docs/specifications/index
   modbot-docs/troubleshooting/index
   modbot-docs/faq/index
   modbot-docs/glossary/index

.. image:: modbot-docs/getting-started/_images/_getting-started.png
.. image:: modbot-docs/getting-started/_images/_hardware.png
.. image:: modbot-docs/getting-started/_images/_software.png
