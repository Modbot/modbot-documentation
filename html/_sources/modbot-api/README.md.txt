# Modbot Protobuf API

Copyright Modbot Inc.
Contact: antonia@modbot.com  

This repo contains the source and recipes for the Modbot API in C++ and Python.
The Modbot API provides a simple interface for communicating with a Modbot Brain and controlling Modbot-compatible software and hardware. It is currently available in C++ and Python flavors.

## About

For documentation, please compile the html documentation source with the target `make docs`. INSTALL, USAGE and CHANGELOG documents are available in the `docs` directory.


## Repository Notes

### Conanfile

There are two conanfiles in this project; `conanfile.py` 
and `conanfile.txt`. 
The reason for the redundant files is to differentiate package dependency import directories. For a conan package build, imports are copied to the build folder. For a local build, imports are copied to the root folder, one level down from the build folder. This is due to the fact that this package has two compilation stages (one to compile the protobuf definitions into *_pb.cc files, and one to compile these generated along with the wrapper into a C++ library).