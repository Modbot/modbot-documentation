=============
Installation
=============

Getting the Source
==================
Clone the latest stable release:

* ``git clone https://bitbucket.org/Modbot/modbot-api.git``


Dependencies
============

The Modbot API relies on several open source libraries.

* Protobuf (3.5.0)
* gRPC (1.9.1)

These dependencies are easily installed locally with the Conan package manager; see sections below for more details.


Quickstart
===========

Install Dependencies
--------------------

* Install pip.
* Install Conan.

  * ``python -m pip install conan``

* Add the Modbot Conan remote.

  * ``conan remote add modbot https://conan.modbot.com``

* Install the library dependencies through Conan.

  * ``make install-dependencies``


C++ Library
-----------

Option 1: Installation as Conan Package
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* Add this package as a dependency to your Conan-compatible project:

  * Add ``modbot-api/[version]@modbot/public`` to your ``conanfile.txt`` or ``conanfile.py``.
  * See _conan.io for more information on the Conan Package Manager.

.. _conan.io: https://conan.io/

* Build or download our package:

  1) Retrieve the prebuilt binary or recipe from our remote:

    * Add our remote with ``conan remote add modbot https://conan.modbot.com``.
    * Run ``conan install`` on your package conanfile to pull down our binary / recipe from the remote.

  2) Build a local conan package from source:

    * Run ``conan create . modbot/public`` in this repo.

Option 2: Installation from Source
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

* ``make cpp`` to install the library system-wide.


Python Library
--------------

Requirements
~~~~~~~~~~~~
Python apps using the python modbot-api library will require additional packages:

* grpcio
* grpcio-tools
* protobuf

Install with ``make python-runtime-dependencies`` or run ``[sudo] python -m pip install protobuf grpcio grpcio-tools``.


Building and Installing the Python Library
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Quick:

* ``make python`` to install the library system-wide.

Advanced:

* ``make clean``
* ``make install-dependencies`` to install the conan requirements.
* ``make python-library-generate`` to generate the python protobuf description files.
* ``make python-library-install`` to install the description files and wrapper library system-wide.

