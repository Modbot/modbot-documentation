Overview of Modbot Platform
-------------------

Modbot is the worlds first common language for robotics hardware and software. It comprises of high quality low cost hardware and software modules and allows anyway to build and deploy robots.

.. image:: images/overview_modbot-architecture
