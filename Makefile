.PHONY: Makefile clean git-submodules install-dependencies modbot-api submodules

OS:=$(shell uname -s)
LLP="LD_LIBRARY_PATH"

ifeq ($(OS),Darwin)
	LLP="DYLD_LIBRARY_PATH"
endif

# Configcd cd
PWD												= $(shell pwd)
BUILD_DIR     						= build
DOC_SOURCE_DIR    				= source

# Sphinx
SPHINX_OPTS    						=
SPHINX_BUILD   						= sphinx-build
SPHINX_PROJ    						= ModbotDocs

clean:
	rm -rf $(BUILD_DIR)

install-dependencies:
	python -m pip install --user -r requirements.txt


git-submodules:
	git submodule init
	git submodule update

modbot-api:
	cd source/modbot-api/docs && $(MAKE) source

submodules: git-submodules modbot-api

%: clean submodules
	mkdir -p build/$@
	$(SPHINX_BUILD) -b $@ "$(DOC_SOURCE_DIR)" "$(BUILD_DIR)/$@" $(SPHINX_OPTS) $(O)