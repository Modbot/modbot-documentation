# Modbot Documentation

This repo contains Modbot support documentation, written in Markdown and ReStructuredText, as well as the sphinx configuration required to build a documentation website. This repo also contains documented subprojects as git submodules.


## About

### Authors

- Antonia Elsen - antonia@modbot.com

### Requirements

Instructions for installing the requirements are listed in the Usage section.

#### Project Requirements

- sphinx
- sphinx extensions:
  - recommonmark

#### Subproject Requirement

- doxygen
- sphinx extensions:
  - sphinx-apidoc
  - breathe

## Usage

This repository is organized as follows:

- `build/` contains any generated documentation; e.g. the generated site html.
- `source/` contains all documentation source.
- `source/conf.py` contains the configuration for sphinx.
- `Makefile` contains the recipe to build all documentation.

To install dependencies:

- install doxygen. On Ubuntu, run `sudo apt-get install doxygen`. On OSX, run `brew install doxygen` and, if necessary, `brew link doxygen`.
- `make install-dependencies` will install all pip package dependencies.

To generate documentation:

- `make html`
  - This target will clean the `build` directory, update all git submodules, then generate documentation according to `source/conf.py`.